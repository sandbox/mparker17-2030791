/**
 * @file
 * Client-side component of the behavior that centers the map on the user's
 * GeoIP data, which is passed from the server session to the page.
 */

/**
 * Behaviour to pass the GeoIP session data to the map and center the map on
 * it.
 */
Drupal.openlayers.addBehavior('openlayers_geoip_center_behavior_geolocate', function (data, options) {
  var location = new OpenLayers.LonLat(
    Drupal.settings.openlayers_geoip_center.longitude,
    Drupal.settings.openlayers_geoip_center.latitude
  ).transform(
    new OpenLayers.Projection("EPSG:4326"),
    data.openlayers.getProjectionObject()
  );

  // Re-centre the map on the point passed into the page.
  data.openlayers.setCenter(location, options.zoom_level);

  // If we should output a point where the user is, do so.
  if (Drupal.settings.openlayers_geoip_center.display_location) {
    // Create a layer and add it to the map.
    var dataLayer = new OpenLayers.Layer.Vector(Drupal.t('User location layer'), {
      projection: new OpenLayers.Projection('EPSG:4326'),
      drupalID: 'openlayers_geoip_center_geolocate_layer'
    });
    styles = Drupal.openlayers.getStyleMap(data.map, 'openlayers_geoip_center');
    dataLayer.styleMap = new OpenLayers.StyleMap({
      'default': styles.styles.openlayers_geoip_center
    });
    data.openlayers.addLayer(dataLayer);

    // Draw a point and ensure it's converted to the right projection.
    var wktFormat = new OpenLayers.Format.WKT();
    var features = wktFormat.read('POINT (' +
      Drupal.settings.openlayers_geoip_center.longitude +
      ', ' +
      Drupal.settings.openlayers_geoip_center.latitude +
      ')'
    );
    if (features.constructor == Array) {
      for (var i in features) {
        features[i].geometry = features[i].geometry.transform(
          new OpenLayers.Projection('EPSG:4326'),
          data.openlayers.projection
        );
      }
    }
    else {
      features.geometry = features.geometry.transform(
        new OpenLayers.Projection('EPSG:4326'),
        data.openlayers.projection
      );
      features = [features];
    }

    // Add the point to the layer.
    dataLayer.addFeatures(features);
  }
});
