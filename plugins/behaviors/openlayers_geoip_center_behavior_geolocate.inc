<?php

/**
 * Pass the GeoIP session data to the map and center the map on it.
 *
 * Server-side component of the behavior that centers the map on the user's
 * GeoIP data, which is passed from the server session to the page.
 */
class openlayers_geoip_center_behavior_geolocate extends openlayers_behavior {

  /**
   * {@inheritdoc}
   */
  public function options_init() {
    return array(
      'bind' => TRUE,
      'watch' => FALSE,
      'geolocationOptions' => array(
        'enableHighAccuracy' => FALSE,
        'maximumAge' => 0,
        'timeout' => 7000,
      ),
      'zoom_level' => 12,
      'location_style' => OPENLAYERS_GEOIP_CENTER_NONE_STYLE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function options_form($defaults = array()) {
    $intials = $this->options_init();
    $form = array();

    $form['bind'] = array(
      '#title' => t('Center when located'),
      '#type' => 'select',
      '#options' => array(
        TRUE => t('Yes'),
        FALSE => t('No'),
      ),
      '#description' => t('When enabled, if the geolocation control finds a location, it will set the center of the map at this point.'),
      '#default_value' => isset($defaults['bind']) ? $defaults['bind'] : $intials['bind'],
    );

    $form['zoom_level'] = array(
      '#title' => t('Zoom level'),
      '#type' => 'textfield',
      '#description' => t('An integer zoom level for when a location is found.  0 is the most zoomed out and higher numbers mean more zoomed in (the number of zoom levels depends on your map).'),
      '#default_value' => isset($defaults['zoom_level']) ? $defaults['zoom_level'] : $intials['zoom_level'],
    );

    $form['location_style'] = array(
      '#type' => 'select',
      '#title' => t("Style for user's location"),
      '#options' => _openlayers_geoip_center_get_style_options(TRUE),
      '#default_value' => isset($defaults['location_style']) ? $defaults['location_style'] : $intials['location_style'],
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function js_dependency() {
    return array(
      'OpenLayers.LonLat',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render(&$map) {
    $display_location = ($this->options['location_style'] !== OPENLAYERS_GEOIP_CENTER_NONE_STYLE);

    // If we're supposed to display the user's current location, make sure that
    // OpenLayers includes the requested style in it's output.
    if ($display_location) {
      $map['styles']['openlayers_geoip_center'] = $this->options['location_style'];
    }

    // Make sure we include the client-side code that does all the work.
    drupal_add_js(drupal_get_path('module', 'openlayers_geoip_center') .
      '/plugins/behaviors/openlayers_geoip_center_behavior_geolocate.js');

    // Send any data that the client-side code will need.
    drupal_add_js(array(
      'openlayers_geoip_center' => array(
        'latitude' => isset($_SESSION['geoip']['data']['latitude']) ?
        $_SESSION['geoip']['data']['latitude'] : $_SESSION['geoip']['data'][0],
        'longitude' => isset($_SESSION['geoip']['data']['longitude']) ?
        $_SESSION['geoip']['data']['longitude'] : $_SESSION['geoip']['data'][1],
        'display_location' => $display_location,
      ),
    ), 'setting');

    // Return the options.
    return $this->options;
  }

}
